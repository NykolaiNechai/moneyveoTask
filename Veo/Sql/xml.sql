DECLARE @xml xml
SET @xml = N'<?xml version="1.0" encoding="utf-16"?><SmsMessage xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><ApplicationId>0</ApplicationId><UserId>0</UserId><AdminId xsi:nil="true" /><RetryCount>5</RetryCount><CheckStatusRetryCount>0</CheckStatusRetryCount><UserGuid xsi:nil="true" /><ApplicationGuid xsi:nil="true" /><TaskId>88357</TaskId><TaskType>Sms</TaskType><NotificationTemplateId xsi:nil="true" /><SmsTemplateId>11</SmsTemplateId><EmailTemplateId xsi:nil="true" /><VoiceMessageTemplateId xsi:nil="true" /><PostalLetterTemplateId xsi:nil="true" /><PushTemplateId xsi:nil="true" /><PromoCompanyId xsi:nil="true" /><CollectionActionId xsi:nil="true" /><PlannedDateTime xsi:nil="true" /><ActionType>CreateTask</ActionType><TaskPriority>2</TaskPriority><VoiceMessageType>Undefined</VoiceMessageType><FailedSubs /><PhoneNumber>937130867</PhoneNumber><Message>Ma kich hoat cua ban: 4276. https://moneyveo.vn// - vay tin dung truc tuyen nhanh chong</Message><UseAlternateSmsProvider>false</UseAlternateSmsProvider><SmsProviderServiceTypeId>SouthTelecomLombardSend</SmsProviderServiceTypeId><ProviderMessageType>Default</ProviderMessageType></SmsMessage>'

select 
 n.value('(./RetryCount/text())[1]','Varchar(50)') as CODE
from @xml.nodes('/SmsMessage') as a(n) 

DECLARE @xmlDoc xml,
@retryCount int = 0

DECLARE offers_cursor CURSOR FOR select dbo.VeoUnzipBytes(BodyCompressed) from data.SchedulerTasks where TaskType = 2 and PlannedTime between '2020-06-23 8:14:11.450' and '2020-06-23 17:13:08.507' order by id desc

OPEN offers_cursor
FETCH NEXT FROM offers_cursor INTO @xmlDoc
WHILE @@FETCH_STATUS = 0 
BEGIN
	select @retryCount = n.value('(./RetryCount/text())[1]','int') from @xmlDoc.nodes('/SmsMessage') as a(n);
	if @retryCount > 4 begin print @retryCount end 

	FETCH NEXT FROM offers_cursor INTO @xmlDoc
END
CLOSE offers_cursor
DEALLOCATE offers_cursor