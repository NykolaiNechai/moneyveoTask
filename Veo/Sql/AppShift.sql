use Moneyveo
go

declare @id int;
declare @term int;
set @term=1;
set @id=4;
update data.Applications set Date= DATEADD(day,-1*(@term),Date),EndTermDate = DATEADD(day,-1*(@term),EndTermDate),LastLoanCalculationDateTime = DATEADD(day,-1*(@term),LastLoanCalculationDateTime)
where id = @id
update data.LoanProlongations 
set ProlongationDate = DATEADD(day,-1*(@term),ProlongationDate),
Created= DATEADD(day,-1*(@term),Created) 
where ApplicationId = @id
update audit.ApplicationStatusChanges
set Created= DATEADD(day,-1*(@term),Created) 
where ApplicationId = @id
update data.RestructuringPlans
set Created= DATEADD(day,-1*(@term),Created),
Date= DATEADD(day,-1*(@term),Date)
where ApplicationId = @id
update data.RestructuringRequests
set Created= DATEADD(day,-1*(@term),rr.Created),
StartDate= DATEADD(day,-1*(@term),StartDate)
from data.RestructuringPlans as rp
join data.RestructuringRequests as rr
on rp.RestructuringRequestId = rr.Id
where rp.ApplicationId = @id
update data.LoanTransactions set Date= DATEADD(day,-1*(@term),Date), Created= DATEADD(day,-1*(@term),Created),AccrualDate = IIF(AccrualDate is null,null, DATEADD(day,-1*(@term),AccrualDate)) where ApplicationId = @id
update data.AgentApplicationRecords set Created= DATEADD(day,-1*(@term),Created), OperationDate= DATEADD(day,-1*(@term),OperationDate), UnbindingExpectedDate= DATEADD(day,-1*(@term),UnbindingExpectedDate) where ApplicationId = @id
update payments.PaymentQueueElements set Created= DATEADD(day,-1*(@term),Created) where ApplicationId = @id
update data.InstallmentPaymentSchedules set Created= DATEADD(day,-1*(@term),Created) where ApplicationId = @id
update data.InstallmentPaymentSchedules set CalculateDate = DATEADD(day,-1*(@term),CalculateDate) where ApplicationId = @id
update data.InstallmentPaymentSchedules set StartPlanDay = DATEADD(day,-1*(@term),StartPlanDay) where ApplicationId = @id

update data.InstallmentPaymentSteps set StartDate = DATEADD(day,-1*(@term),StartDate)
where Id in (
select ps.Id from data.InstallmentPaymentSteps ps
join data.InstallmentPaymentSchedules ips on ps.InstallmentPaymentScheduleId = ips.Id
where ips.ApplicationId = @id
group by ps.Id
)