use Moneyveo
go
select top 100 * from data.Users order by id desc;
select * from payments.PaymentQueueElements order by id desc


use MoneyVeo_PreProd
go

select * from data.Resources r
--where r.Type = 1 order by r.Id
join data.ResourceValues rv on rv.ResourceId = r.Id --and LanguageId = 1
--join data.ResourceLanguages rl on rl.Id = rv.LanguageId 
where 
rv.Value like N'%You can reschedule credit payment%'
r.Name in ('WrongLastName') order by r.Name, LanguageId

select * from data.CollectionStages

/*
delete from data.ResourceValues where id = 67473;
delete from data.Resources where Id = 31568;
*/

/***** Script for SelectTopNRows command from SSMS  *****/
SELECT TOP (1000) * FROM [Moneyveo_PreProd].[data].[Resources] a
Join [Moneyveo_PreProd].[data].[ResourceValues] b ON a.Id = b.ResourceId
Join [Moneyveo_PreProd].[data].ResourceLanguages c ON b.LanguageId = c.Id
WHERE b.Value NOT LIKE '%??%' AND b.LanguageId = 1

select * from config.SecretQuestions sq
join data.Resources r on r.Id = sq.TitleId
left join data.ResourceValues rv on rv.ResourceId = r.Id

select * from settings.Settings where KeyName = 'SystemCountry'

select  Name, TwoLetterIsoLanguageName, Culture, DisplayName from data.ResourceLanguages;

--UPDATE data.Resources SET Type = 1 WHERE Name = 'BottomLogoTitle'

select * from data.ContentItems a join data.ContentItemTypes b on a.TypeId = b.Id
where b.Name = 'AboutCategory';

select * from data.ContentItems a join data.ContentItemTypes b on a.TypeId = b.Id
join [data].[Resources] x on a.TitleId = x.Id
Join [data].[ResourceValues] y ON x.Id = y.ResourceId
where a.ParentId = 24765

select * from data.LoanTransactions a join data.CompanyTypeTransactionTypes b on a.TransactionTypeId = b.TransactionTypeId