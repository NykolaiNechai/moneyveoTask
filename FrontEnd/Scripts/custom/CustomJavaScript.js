﻿var CustomJavaScript = (function () {
    var initUI = function () {
        $(function () {
            $("#btn0").click(function () {
                Export();
            });
        });

        $(function () {
            $("#btnTurnLeft").click(function () {
                RoatMatrix('/Home/TurnLeft');
            });
        });

        $(function () {
            $("#btnTurnRight").click(function () {
                RoatMatrix('/Home/TurnRight');
            });
        });

        $(function () {
            $("#btn3").click(function () {
                GeMatrix();
            });
        });

        function RoatMatrix(urlhandler) {
            var dict = new Object();
            $("#tabledata tr").each(
                function (index, item) {
                    inputs = $(this).find("input:text");
                    var list = new Array();
                    dict["matrix[" + index + "].Key"] = index;
                    inputs.each(
                        function (index, item) {
                            list[index] = $(this).val();
                        });
                    dict["matrix[" + index + "].Value"] = list;
                });

            $.ajax({
                type: 'POST',
                cache: false,
                traditional: true,
                url: urlhandler,
                dataType: 'json',
                data: dict,
                success: function (response) {
                    $('#turned').html(response.data);
                    $('#turnResult').prev().text('Result: ' + urlhandler);
                },
                error: function () {
                    alert('Ajax error!');
                }

            });
        }

        function GeMatrix() {
            $.ajax({
                url: '/Home/GetRndMatrix',
                type: 'POST',
                success: function (data) {
                    $('#tabledata').html(data.data);
                },
                error: function () {
                    alert('Ajax error!');
                }
            });
        }

        function Export() {
            var dict = new Object();
            $("#turned tr").each(
                function (index, item) {
                    inputs = $(this).find("input:text");
                    var list = new Array();
                    dict["matrix[" + index + "].Key"] = index;
                    inputs.each(
                        function (index, item) {
                            list[index] = $(this).val();
                        });
                    dict["matrix[" + index + "].Value"] = list;
                });

            $.ajax({
                type: 'POST',
                cache: false,
                traditional: true,
                url: '/Home/ExportMatrix',
                dataType: 'json',
                data: dict,
            }).done(function (data) {
                if (data.fileName !== "") {
                    window.location.href = "@Url.RouteUrl(new { Controller = 'Home', Action = 'Download'})/?file=" + data.fileName;
                }
            });
        }
    }

    return {
        init: function () {
            initUI();
        },
    };
}());