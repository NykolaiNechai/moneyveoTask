﻿using System.Collections.Generic;

namespace FrontEnd.Models {
    public class MatrixViewModel
    {
        public Dictionary<int, int[]> Matrix { get; set; }
    }
}