﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FrontEnd.Util.IoC;
using FrontEnd.Util.MappingProfiles;

namespace FrontEnd
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutofacConfig.ConfigureContainer();
            AutoMapper.Mapper.Initialize(x => x.AddProfile<ViewModelProfile>());
        }
    }
}
