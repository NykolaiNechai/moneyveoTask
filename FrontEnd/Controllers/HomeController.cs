﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using FrontEnd.Domain;
using FrontEnd.Models;

namespace FrontEnd.Controllers {
    public class HomeController : Controller {
        private IRandomMatrixService _rndSvc;
        private IExcellMatrixService _excellSvc;
        private IOperationService _operSvc;

        public HomeController(IRandomMatrixService rndSvc, IExcellMatrixService excellSvc, IOperationService operSvc) {
            _rndSvc = rndSvc;
            _excellSvc = excellSvc;
            _operSvc = operSvc;
        }

        private MatrixViewModel GetDefaultMatrix() {
            var matrix = _rndSvc.GetMatrix();
            var model = Mapper.Map<MatrixViewModel>(matrix);

            return model;
        }

        private string ConvertViewToString(string viewName, object model) {
            ViewData.Model = model;
            using (StringWriter writer = new StringWriter()) {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }

        [HttpGet]
        public ViewResult Index() {
            return View(GetDefaultMatrix());
        }

        [HttpPost]
        public ActionResult TurnLeft(IDictionary<int, int[]> matrix) {
            var request = Mapper.Map<OperationServiceRequest>(matrix);
            var responce = _operSvc.TurnLeft(request);
            var model = responce.Matrix;

            if (Request != null && Request.IsAjaxRequest()) {
                return Json(new { error = true, data = ConvertViewToString("MatrixData", model) });
            }

            return PartialView("MatrixData", model);
        }

        [HttpPost]
        public ActionResult TurnRight(IDictionary<int, int[]> matrix) {
            var request = Mapper.Map<OperationServiceRequest>(matrix);
            var responce = _operSvc.TurnRight(request);
            var model = responce.Matrix;

            if (Request != null && Request.IsAjaxRequest()) {
                return Json(new { error = true, data = ConvertViewToString("MatrixData", model) });
            }

            return PartialView("MatrixData", model);
        }

        [HttpPost]
        public ActionResult GetRndMatrix() {
            var model = GetDefaultMatrix();
            var rr = Json(new { error = true, data = ConvertViewToString("MatrixData", model.Matrix) });
            return rr;
        }

        [HttpPost]
        public ActionResult Upload() {
            if (Request.Files.Count > 0) {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0) {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                    file.SaveAs(path);

                    _excellSvc.FilePath = path;
                    var matrix = _excellSvc.GetMatrix();
                    var model = Mapper.Map<MatrixViewModel>(matrix);

                    return View("Index", model);
                }
            }

            return View("Error");
        }

        public ActionResult ExportMatrix(IDictionary<int, int[]> matrix) {
            var name = string.Format("{0}.csv", Guid.NewGuid().ToString());
            _operSvc.ExportMatrix(Path.Combine(Server.MapPath("~/Uploads/"), name), matrix);
            return Json(new { fileName = name, errorMessage = "" });
        }

        public ActionResult Download(string file) {
            var fullPath = Path.Combine(Server.MapPath("~/Uploads/"), file);
            string file_type = "text/csv";
            string file_name = "Excell.csv";

            return File(fullPath, file_type, file_name);
        }
    }
}