﻿using AutoMapper;
using System.Collections.Generic;
using FrontEnd.Models;
using FrontEnd.Domain;

namespace FrontEnd.Util.MappingProfiles
{
    public class ViewModelProfile : Profile
    {
        public ViewModelProfile()
        {
            CreateMap<Dictionary<int, int[]>, MatrixViewModel>()
                .ForMember(x => x.Matrix, y => y.MapFrom(c => c));

            CreateMap<Dictionary<int, int[]>, OperationServiceRequest>()
                .ForMember(x => x.Matrix, y => y.MapFrom(c => c));

            ////CreateMap<OperationServiceResponce, Dictionary<int, int[]>>()
            ////    .ForMember(x => x, y => y.MapFrom(c => c.Matrix));
        }
    }
}