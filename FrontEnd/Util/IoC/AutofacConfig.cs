﻿using Autofac;
using Autofac.Integration.Mvc;
using System.Web.Mvc;
using FrontEnd.Domain;

namespace FrontEnd.Util.IoC {
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);            

            builder.RegisterType<RandomMatrixService>().As<IRandomMatrixService>().InstancePerRequest();
            builder.RegisterType<ExcellMatrixService>().As<IExcellMatrixService>().InstancePerRequest();
            builder.RegisterType<OperationService>().As<IOperationService>().InstancePerRequest(); 

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}