﻿using System.Collections.Generic;
using System.IO;

namespace FrontEnd.Domain {
    public class OperationService : IOperationService {
        public OperationServiceResponce TurnLeft(OperationServiceRequest request) {
            var matrix = request.Matrix;
            var m = matrix[0].Length;

            for (int k = 0; k < m / 2; k++) {
                for (int j = k; j < m - 1 - k; j++) {
                    int tmp = matrix[k][j];
                    matrix[k][j] = matrix[j][m - 1 - k];
                    matrix[j][m - 1 - k] = matrix[m - 1 - k][m - 1 - j];
                    matrix[m - 1 - k][m - 1 - j] = matrix[m - 1 - j][k];
                    matrix[m - 1 - j][k] = tmp;
                }
            }

            var responce = new OperationServiceResponce() { Matrix = matrix };

            return responce;
        }
        public OperationServiceResponce TurnRight(OperationServiceRequest request) {
            var m = request.Matrix;
            var n = m[0].Length;

            for (int i = 0; i < n / 2; i++) {
                for (int j = i; j < n - 1 - i; j++) {
                    int tmp = m[i][ j];
                    m[i][j] = m[n - 1 - j][i];
                    m[n - 1 - j][i] = m[n - 1 - i][n - 1 - j];
                    m[n - 1 - i][n - 1 - j] = m[j][n - 1 - i];
                    m[j][n - 1 - i] = tmp;
                }
            }

            var responce = new OperationServiceResponce() { Matrix = m };

            return responce;
        }

        public void ExportMatrix(string filePath, IDictionary<int, int[]> matrix) {
            using (var fileSw = new StreamWriter(filePath, false)) {
                for (int k = 0; k < matrix.Keys.Count; k++) {
                    var arr = matrix[k];
                    foreach (var item in arr) {
                        fileSw.Write(string.Format("{0},", item));
                    }

                    fileSw.WriteLine();
                }
            }
        }
    }
}