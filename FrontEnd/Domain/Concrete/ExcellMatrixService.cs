﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FrontEnd.Domain {
    public class ExcellMatrixService : MatrixService, IExcellMatrixService {
        public string FilePath { get; set; }

        protected override void BuildMatrix() {
            this.matrix = new Dictionary<int, int[]>();

            string filePath = FilePath;

            using (StreamReader sr = new StreamReader(filePath)) {
                int row = 0;
                while (!sr.EndOfStream) {
                    int[] arr = sr.ReadLine().Split(',').Select(x => { int t; int.TryParse(x, out t); return t; }).ToArray();
                    this.matrix.Add(row, arr);
                    row++;
                }
            }
        }
    }
}