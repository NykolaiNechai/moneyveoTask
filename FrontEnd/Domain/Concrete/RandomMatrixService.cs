﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace FrontEnd.Domain
{
    public class RandomMatrixService : MatrixService, IRandomMatrixService
    {
        private const int MIN = 0;
        private const int MAX = 100;
        private const int MAX_MATRIX_SIZE = 8;

        protected override void BuildMatrix()
        {
            var rnd = new Random();
            var n = rnd.Next(MAX_MATRIX_SIZE);

            this.matrix = new Dictionary<int, int[]>();

            for (int k = 0; k < n; k++)
            {
                var arr = new int[n];

                for (int i = 0; i < arr.Length; i++)
                {
                    ////Thread.Sleep(100);
                    arr[i] = rnd.Next(MIN, MAX);
                }

                this.matrix.Add(k, arr);
            }
        }
    }
}