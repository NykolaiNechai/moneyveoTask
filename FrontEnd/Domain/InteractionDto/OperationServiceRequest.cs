﻿using System.Collections.Generic;

namespace FrontEnd.Domain {
    public class OperationServiceRequest
    {
        public Dictionary<int, int[]> Matrix { get; set; }
    }
}