﻿using System.Collections.Generic;
using System.Linq;

namespace FrontEnd.Domain {
    public abstract class MatrixService
    {
        protected Dictionary<int, int[]> matrix;

        public MatrixService() { }

        protected abstract void BuildMatrix();

        public Dictionary<int, int[]> GetMatrix()
        {
            if (this.matrix == null || !matrix.Any())
            {
                BuildMatrix();
            }

            return this.matrix;
        }        
    }
}