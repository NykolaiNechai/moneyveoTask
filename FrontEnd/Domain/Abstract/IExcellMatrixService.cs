﻿using System.Collections.Generic;

namespace FrontEnd.Domain {
    public interface IExcellMatrixService
    {
        string FilePath { get; set; }

        Dictionary<int, int[]> GetMatrix();
    }
}
