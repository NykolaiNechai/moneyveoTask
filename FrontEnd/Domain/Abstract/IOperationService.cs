﻿using System.Collections.Generic;

namespace FrontEnd.Domain {
    public interface IOperationService
    {
        OperationServiceResponce TurnLeft(OperationServiceRequest request);
        OperationServiceResponce TurnRight(OperationServiceRequest request);

        void ExportMatrix(string filePath, IDictionary<int, int[]> matrix);
    }
}
