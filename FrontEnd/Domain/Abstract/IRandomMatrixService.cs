﻿using System.Collections.Generic;

namespace FrontEnd.Domain {
    public interface IRandomMatrixService
    {
        Dictionary<int, int[]> GetMatrix();
    }
}
