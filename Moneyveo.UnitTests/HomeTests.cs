﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using FrontEnd.Controllers;
using FrontEnd.Domain;
using FrontEnd.Models;
using FrontEnd.Util.MappingProfiles;

namespace Moneyveo.UnitTests {

    [TestClass]
    public class HomeTests {
        private static Dictionary<int, int[]> matrix = new Dictionary<int, int[]>();
        private static Mock<IRandomMatrixService> _rndSvc;
        private static Mock<IExcellMatrixService> _excellSvc;
        private static Mock<IOperationService> _operSvc;

        [AssemblyInitialize]
        public static void StartUp(TestContext context) {
            matrix.Add(0, new int[] { 25, 38, 12 });
            matrix.Add(1, new int[] { 1, 7, 76 });
            matrix.Add(2, new int[] { 21, 64, 54 });

            _rndSvc = new Mock<IRandomMatrixService>();
            _excellSvc = new Mock<IExcellMatrixService>();
            _operSvc = _operSvc = new Mock<IOperationService>();

            _rndSvc.Setup(x => x.GetMatrix()).Returns(() => {
                return matrix;
            });

            var resp = new OperationServiceResponce() { Matrix = matrix };

            _operSvc.Setup(x => x.TurnLeft(It.IsAny<OperationServiceRequest>())).Returns(resp);
            _operSvc.Setup(x => x.TurnRight(It.IsAny<OperationServiceRequest>())).Returns(resp);

            Mapper.Initialize(x => x.AddProfile<ViewModelProfile>());
        }

        [TestMethod]
        public void Home_Index_Content() {
            var homeController = new HomeController(_rndSvc.Object, null, null);
            var result = (MatrixViewModel)homeController.Index().ViewData.Model;

            Assert.IsNotNull(result.Matrix);
            Assert.IsTrue(result.Matrix.Any());
        }

        [TestMethod]
        public void Home_TurnLeft_Content() {
            var homeController = new HomeController(_rndSvc.Object, null, _operSvc.Object);
            var result = homeController.TurnLeft(matrix);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Home_TurnRight_Content() {
            var homeController = new HomeController(_rndSvc.Object, null, _operSvc.Object);
            var result = homeController.TurnRight(matrix);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [Ignore]
        public void Home_GetRndMatrix_Content() {
            var server = new Mock<HttpServerUtilityBase>();
            server.Setup(x => x.MapPath(It.IsAny<string>())).Returns("c:\\temp\\");

            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Server).Returns(server.Object);
            httpContext.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/home/GetRndMatrix");

            var homeController = new HomeController(_rndSvc.Object, null, null);
            homeController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), homeController);

            var result = homeController.GetRndMatrix();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Home_ExportMatrix_Content() {
            var server = new Mock<HttpServerUtilityBase>();
            server.Setup(x => x.MapPath(It.IsAny<string>())).Returns("c:\\temp\\");

            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Server).Returns(server.Object);

            var homeController = new HomeController(null, null, _operSvc.Object);
            homeController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), homeController);

            var result = (JsonResult)homeController.ExportMatrix(matrix);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void OperationService_TurnLeft() {
            var svc = new OperationService();
            var request = Mapper.Map<OperationServiceRequest>(matrix);
            var result = svc.TurnLeft(request);

            Assert.IsTrue(result.Matrix[0][0] == 12);
            Assert.IsTrue(result.Matrix[0][1] == 76);
            Assert.IsTrue(result.Matrix[0][2] == 54);
            Assert.IsTrue(result.Matrix[1][0] == 38);
            Assert.IsTrue(result.Matrix[1][1] == 7);
            Assert.IsTrue(result.Matrix[1][2] == 64);
            Assert.IsTrue(result.Matrix[2][0] == 25);
            Assert.IsTrue(result.Matrix[2][1] == 1);
            Assert.IsTrue(result.Matrix[2][2] == 21);            
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void OperationService_TurnRight() {
            var svc = new OperationService();
            var request = Mapper.Map<OperationServiceRequest>(matrix);
            var result = svc.TurnRight(request);

            Assert.IsTrue(result.Matrix[0][0] == 21);
            Assert.IsTrue(result.Matrix[0][1] == 1);
            Assert.IsTrue(result.Matrix[0][2] == 25);
            Assert.IsTrue(result.Matrix[1][0] == 64);
            Assert.IsTrue(result.Matrix[1][1] == 7);
            Assert.IsTrue(result.Matrix[1][2] == 38);
            Assert.IsTrue(result.Matrix[2][0] == 54);
            Assert.IsTrue(result.Matrix[2][1] == 76);
            Assert.IsTrue(result.Matrix[2][2] == 12);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void OperationService_ExportMatrix() {

            var directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var path = string.Format("{0}\\{1}", directory, "Excell_tmp.csv");

            var svc = new OperationService();
            svc.ExportMatrix(path, matrix);

            Assert.IsTrue(File.Exists(path));
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void RandomMatrixService_GetMatrix() {
            var svc = new RandomMatrixService();
             var result = svc.GetMatrix();

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(Dictionary<int, int[]>));
            Assert.IsTrue(result[0][0].GetType() == typeof(int));            
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ExcellMatrixService_GetMatrix() {

            var directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var path = string.Format("{0}\\{1}", directory, "Excell (7).csv");

            var svc = new ExcellMatrixService();
            svc.FilePath = path;
            var result = svc.GetMatrix();

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(Dictionary<int, int[]>));
            Assert.IsTrue(result[0][0].GetType() == typeof(int));
        }
    }
}
